import igIcon from './ig.svg'
import mailIcon from './mail.svg'
import phoneIcon from './phone.svg'
import teleIcon from './tele.svg'
import ytIcon from './yt.svg'

export {igIcon,mailIcon,phoneIcon,teleIcon,ytIcon}
import banner from "./banner.png";
import hero from "./hero.png";
import image from "./image.png";
import logo from "./logo.png";
import successimg from "./success.svg";

export { banner, hero, image, logo, successimg };
